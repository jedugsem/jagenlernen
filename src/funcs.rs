use serde::de::DeserializeOwned;
use serde::Serialize;

pub async fn fetch<E, T>(url: String, body: Option<E>) -> Result<T, String>
where
    T: DeserializeOwned,
    E: Serialize,
{
    let mut bin = vec![];
    if let Some(data) = &body {
        bin = bincode::serialize(data).unwrap();
    }
    let request = reqwest::Client::new().get(&url).body(bin).send().await;
    if let Ok(resp) = request {
        if let Ok(bin) = resp.bytes().await {
            //Ok(repo)

            let decoded: Result<T, Box<bincode::ErrorKind>> = bincode::deserialize(&bin[..]);
            if let Ok(data) = decoded {
                Ok(data)
            } else {
                Err("serde error".to_string())
            }
        } else {
            Err("bin-error".to_string())
        }
    } else {
        Err(format!("{:?}", request))
    }
}
pub async fn post<T>(url: String, data: T) -> Result<String, String>
where
    T: Serialize,
{
    let bin = bincode::serialize(&data).unwrap();
    let response = reqwest::Client::new().post(&url).body(bin).send().await;
    if let Ok(resp) = response {
        if let Ok(text) = resp.text().await {
            println!("{}", text);
            Ok("ok".to_string())
        } else {
            Err("bin-error".to_string())
        }
    } else {
        Err("req-error".to_string())
    }
}
