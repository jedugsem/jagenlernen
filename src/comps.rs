use crate::{Common, Message};
use iced::{
    widget::{button, checkbox, column, row, text, Column, Row},
    Alignment, Element, Length,
};
use jagdlib::types::*;
#[derive(Debug, Default)]
pub struct Quest {
    pub is_checked: [bool; 6],
}
//const ARRAY: &[char] = &['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'];
impl Quest {
    pub fn view(&self, question: Question, theme: String, next_m: Message) -> Column<Message> {
        let quests: Vec<Element<Message>> = question
            .awnsers
            .iter()
            .enumerate()
            .map(|(i, awnser)| {
                checkbox(
                    //format!("{}) {}", ARRAY[i], &awnser.1),
                    format!("{}", &awnser.1),
                    self.is_checked[i],
                    move |is| Message::Toggle(i, is),
                )
                .width(Length::Fill)
                .into()
            })
            .collect();
        let quest: Column<Message> = column![
            text(&question.question).size(25),
            column(quests).spacing(10),
        ]
        .spacing(10)
        .height(Length::Units(300))
        .width(Length::Fill)
        .align_items(Alignment::Start);

        column![
            text(&format!("Fachgebiet: {} Frage {}", theme, question.num + 1,)).size(20),
            quest,
            button("Next").on_press(next_m),
        ]
        .padding(30)
        .spacing(20)
        .align_items(Alignment::Center)
    }
}

#[derive(Default)]
pub struct TopBar {}
impl TopBar {
    pub fn view(&self, m: Message) -> Row<Message> {
        row![
            button("<=").on_press(m),
            button("<H>").on_press(Message::Common(Common::Home))
        ]
        .padding(10)
        .width(Length::Fill)
        .spacing(10)
    }
}
