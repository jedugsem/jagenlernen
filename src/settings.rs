use crate::{comps::TopBar, Common, Message};

use iced::{
    widget::{column, text, Column},
    Alignment, Length,
};
#[derive(Default)]
pub struct Settingst {
    topbar: TopBar,
}
impl Settingst {
    pub fn view(&self) -> Column<Message> {
        column![
            self.topbar.view(Message::Common(Common::Back)),
            column![text("Settings").size(50)].align_items(Alignment::Center),
        ]
        .width(Length::Units(600))
        .align_items(Alignment::Center)
    }
}
