//#[cfg(target_family = "unix")]
//use jagdlib::funcs::*;

use crate::{
    comps::{Quest, TopBar},
    wrapper::*,
    Common, Message,
};
use iced::{
    widget::{
        button,
        //checkbox,
        column,
        pick_list,
        progress_bar,
        text,
        Column,
    },
    Alignment, Command, Length,
};

use jagdlib::types::*;
#[derive(Debug, Clone)]
//#[derive(Clone,Copy)]
pub enum Mlearn {
    Queue,
    Start,
    Setup(Result<MetaDb, String>),
    Pracmenu,
    LoadedNext(Result<Question, String>),
    Test,
    Stats,
    Next,
    Prac,
    Tick,
    Home,
    Mode(String),
}

#[derive(Default)]
pub struct Learn {
    pub state: LearnSt,
    pub topbar: TopBar,
    pub substate: LearnCa,
}
impl Learn {
    pub fn new(wrap: Wrap) -> Self {
        Learn {
            state: LearnSt {
                wrap,
                ..Default::default()
            },
            ..Default::default()
        }
    }
}
pub enum LearnCa {
    Menu(Lmenu),
    Prac(Lprac),
    Test(Ltest),
    Status(Lstatus),
    Card(Lcard),
}
impl Default for LearnCa {
    fn default() -> Self {
        LearnCa::Menu(Lmenu::default())
    }
}
#[derive(Default)]
pub struct Lcard {
    pub card: Quest,
}

pub struct Lprac {
    _selected: Option<String>,
}
impl Default for Lprac {
    fn default() -> Self {
        Lprac {
            _selected: Some("".to_string()),
        }
    }
}
pub struct Ltest {
    minutes: u64,
    seconds: u64,
}
impl Default for Ltest {
    fn default() -> Self {
        Ltest {
            minutes: 0,  //29,
            seconds: 10, //59,
        }
    }
}
#[derive(Default)]
struct TestResults {
    _test: bool,
}
#[derive(Default)]
pub struct Lstatus {}
#[derive(Default)]
pub struct Lmenu {}
pub struct LearnSt {
    meta: MetaDb,
    wrap: Wrap,
    mode: Option<String>,
    modes: Vec<String>,
    progress: f32,
    temp_q: Question,
    error: bool,
    _results: TestResults,
    pub warning: bool,
}
impl Default for LearnSt {
    fn default() -> Self {
        LearnSt {
            _results: TestResults::default(),
            wrap: Wrap::default(),
            meta: MetaDb::default(),
            modes: vec![],
            mode: Some("".to_string()),
            temp_q: Question::default(),
            progress: 0.0,
            warning: false,
            error: false,
        }
    }
}
impl Learn {
    pub fn view(&self) -> Column<Message> {
        match self {
            Learn {
                state: _learnst,
                topbar,
                substate: LearnCa::Test(_test),
            } => {
                let butn = button(text("Start"));
                column![
                    topbar.view(Message::Learn(Mlearn::Home)),
                    text(format!("time left: {}:{}", _test.minutes, _test.seconds)),
                    butn,
                ]
                .align_items(Alignment::Center)
                .width(Length::Units(600))
            }
            Learn {
                state: _learnst,
                topbar,
                substate: LearnCa::Status(_status),
            } => {
                let sdd = text("not good enough yet");
                column![
                    topbar.view(Message::Learn(Mlearn::Home)),
                    progress_bar(0.0..=1.0, _learnst.progress).width(Length::Units(200)),
                    sdd,
                ]
                .align_items(Alignment::Center)
                .width(Length::Units(600))
            }
            Learn {
                state: learnst,
                topbar,
                substate: LearnCa::Prac(_),
            } => {
                let content = column![
                    text("Practice").size(30),
                    pick_list(learnst.modes.clone(), learnst.mode.clone(), |t| {
                        Message::Learn(Mlearn::Mode(t))
                    }),
                    button("Start").on_press(Message::Learn(Mlearn::Prac)),
                    //checkbox("Enable Warning", learnst.warning, Message::LearnWarning)
                    //    .width(Length::Fill),
                ]
                .spacing(20)
                .padding(40)
                .align_items(Alignment::Center);
                column![topbar.view(Message::Learn(Mlearn::Home)), content,]
                    .align_items(Alignment::Center)
                    .width(Length::Units(600))
            }
            Learn {
                state: learnst,
                topbar,
                substate: LearnCa::Card(card),
            } => {
                //if !learnst.error {
                column![
                    topbar.view(Message::Learn(Mlearn::Pracmenu)),
                    text(if learnst.error {
                        "last one was wrong"
                    } else {
                        "last one right"
                    }),
                    card.card.view(
                        learnst.temp_q.clone(),
                        "thema".to_string(),
                        Message::Learn(Mlearn::Next),
                    ),
                ]
                .align_items(Alignment::Center)
                .width(Length::Units(600))
                //} else {
                //    column![text("error please correct")]
                //}
            }
            Learn {
                state: learnst,
                topbar,
                substate: LearnCa::Menu(_menu),
            } => {
                let content = column![
                    text("JagenLernen".to_string()).size(50),
                    progress_bar(0.0..=1.0, learnst.progress).width(Length::Units(200)),
                    button("Practice")
                        .width(Length::Units(200))
                        .on_press(Message::Learn(Mlearn::Pracmenu)),
                    button("Test")
                        .width(Length::Units(200))
                        .on_press(Message::Learn(Mlearn::Test)),
                    button("Stats")
                        .width(Length::Units(200))
                        .on_press(Message::Learn(Mlearn::Stats)),
                ]
                .padding(40)
                .spacing(20)
                .align_items(Alignment::Center);

                column![topbar.view(Message::Common(Common::Home)), content,]
                    .align_items(Alignment::Center)
                    .width(Length::Units(600))
            }
        }
    }
    pub fn update(&mut self, message: Mlearn) -> Command<Message> {
        match message {
            Mlearn::Setup(var) => {
                if let Ok(s) = var {
                    self.state.meta = s;
                    let mut temp = vec![];
                    temp.extend(self.state.meta.thema_names.clone());
                    temp.extend(self.state.meta.extra_modes.clone());
                    self.state.modes = temp.clone();
                    if !temp.is_empty() {
                        self.state.mode = Some(temp[0].clone())
                    }
                } else {
                    println!("eroor with setup{:?}", var);
                }
                Command::none()
            }
            Mlearn::Pracmenu => {
                self.substate = LearnCa::Prac(Lprac::default());
                Command::none()
            }
            Mlearn::Tick => {
                if let LearnCa::Test(ltest) = &mut self.substate {
                    if ltest.seconds > 0 {
                        ltest.seconds -= 1;
                    } else {
                        ltest.seconds += 59;
                        ltest.minutes -= 1;
                    }
                    if ltest.minutes == 0 && ltest.seconds == 0 {
                        self.substate = LearnCa::default();
                    }
                }
                Command::none()
            }
            Mlearn::Test => {
                self.substate = LearnCa::Test(Ltest::default());
                Command::perform(
                    set_mode(self.state.wrap.get_arc(), Mode::Learn("Test".to_string())),
                    |_| Message::Loaded,
                )
            }
            Mlearn::Stats => {
                self.substate = LearnCa::Status(Lstatus::default());
                Command::none()
            }
            Mlearn::Prac => {
                self.substate = LearnCa::Card(Lcard::default());
                Command::perform(
                    set_mode(
                        self.state.wrap.get_arc(),
                        Mode::Learn(self.state.mode.clone().unwrap()),
                    ),
                    |_| Message::Learn(Mlearn::Start),
                )
            }
            Mlearn::Start => Command::perform(next(self.state.wrap.get_arc()), |next| {
                Message::Learn(Mlearn::LoadedNext(next))
            }),
            Mlearn::Mode(t) => {
                self.state.mode = Some(t.clone());
                if let LearnCa::Prac(Lprac { _selected: _ }) = self.substate {
                    self.state.mode = Some(t);
                }
                Command::none()
            }
            Mlearn::Next => {
                let mut command = vec![];
                match self.state.mode.as_ref().unwrap().as_str() {
                    "Test" => {
                        println!("test");
                    }
                    _ => {
                        let mut error = false;
                        if let LearnCa::Card(card) = &self.substate {
                            for (i, _, _) in self.state.temp_q.awnsers.clone().iter() {
                                if self.state.temp_q.awnsers[*i as usize].2
                                    == card.card.is_checked[*i as usize]
                                {
                                } else {
                                    error = true;
                                }
                            }
                        }

                        if !error {
                            let test: u64 = std::time::SystemTime::now()
                                .duration_since(std::time::SystemTime::UNIX_EPOCH)
                                .unwrap()
                                .as_secs();
                            self.state.temp_q.fals = Some(test);
                            command.push(Command::perform(
                                queue(self.state.wrap.get_arc(), self.state.temp_q.clone()),
                                |_| Message::Learn(Mlearn::Queue),
                            ));
                            self.state.error = false;
                        } else {
                            self.state.error = true;
                        }
                    }
                }
                if let LearnCa::Card(card) = &mut self.substate {
                    card.card.is_checked = [false; 6];
                }
                command.push(Command::perform(next(self.state.wrap.get_arc()), |ss| {
                    Message::Learn(Mlearn::LoadedNext(ss))
                }));
                Command::batch(command)
            }
            Mlearn::LoadedNext(var) => {
                if let Ok(s) = var {
                    self.state.temp_q = s;
                } else {
                    self.state.temp_q = jagdlib::types::Question::default();
                }
                Command::none()
            }
            Mlearn::Queue => Command::perform(save(self.state.wrap.get_arc()), |_| Message::Loaded),
            Mlearn::Home => {
                self.substate = LearnCa::default();
                Command::perform(save(self.state.wrap.get_arc()), |_| Message::Loaded)
            }
        }
    }
}
