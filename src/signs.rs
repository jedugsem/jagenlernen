use crate::{comps::TopBar, Common, Message};
use iced::{
    widget::{button, column, pick_list, text, Column},
    Alignment,
    Command,
    Length, //system::Action,
};
use jagdlib::types::AudioItem;
use rand::seq::SliceRandom;

#[derive(Clone, Debug)]
pub enum Msigns {
    Audio(Vec<AudioItem>),
    Pick(AudioItem),
    Play,
    End,
}
pub struct Signs {
    selected: Option<AudioItem>,
    topbar: TopBar,
    right: bool,
    sounds: Vec<AudioItem>,
    playing: bool,
    played: AudioItem,
}
impl Default for Signs {
    fn default() -> Self {
        let vec: Vec<AudioItem> = vec![];
        let mut selected = None;
        if !vec.is_empty() {
            selected = Some(vec[0].clone());
        }
        //leFt (_stream, handle) = rodio::OutputStream::try_default().unwrap();
        Signs {
            selected,
            topbar: TopBar::default(),
            sounds: vec,
            playing: false,
            played: AudioItem::default(),
            right: false,
        }
    }
}
impl Signs {
    pub fn view(&self) -> Column<Message> {
        let tezt = if self.playing {
            text("playing")
        } else {
            text("not playing")
        };
        let content = column![
            text("Signs Test").size(30),
            tezt,
            text(if self.right { "Right" } else { "" }).size(20),
            button("Play-Random").on_press(Message::Signs(Msigns::Play)),
            pick_list(self.sounds.clone(), self.selected.clone(), |t| {
                Message::Signs(Msigns::Pick(t))
            }),
        ]
        .align_items(Alignment::Center)
        .spacing(20);
        column![self.topbar.view(Message::Common(Common::Home)), content,]
            .width(Length::Units(600))
            .align_items(Alignment::Center)
    }
    pub fn update(&mut self, message: Msigns) -> Command<Message> {
        match message {
            Msigns::Audio(audio) => {
                self.sounds = audio.clone();
                if !&audio.is_empty() {
                    self.selected = Some(audio[0].clone());
                }
                Command::none()
            }
            Msigns::Pick(t) => {
                self.selected = Some(t);
                if self.selected.as_ref().unwrap() == &self.played {
                    self.right = true;
                }
                Command::none()
            }
            Msigns::Play => {
                self.playing = true;
                self.right = false;
                let mut rng = rand::thread_rng();

                if let Some(ok) = self.sounds.choose(&mut rng) {
                    self.played = ok.clone();
                }

                //println!("{}", self.played);
                Command::perform(play(self.played.clone()), |_| Message::Signs(Msigns::End))
            }
            Msigns::End => {
                self.playing = false;
                Command::none()
            }
        }
    }
}

async fn play(song: AudioItem) {
    if !song.audio.is_empty() {
        let (_stream, handle) = rodio::OutputStream::try_default().unwrap();
        let sink = rodio::Sink::try_new(&handle).unwrap();
        sink.append(rodio::Decoder::new(std::io::Cursor::new(song.audio)).unwrap());
        sink.sleep_until_end();
    }
}
