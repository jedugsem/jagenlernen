use crate::funcs::*;
use jagdlib::types;
use std::sync::{Arc, Mutex};

pub const WEB: bool = cfg!(feature = "web");
pub const DATA: &str = "/usr/share/jagenlernen/data/bindata";
pub const ROOT: &str = "http://127.0.0.1:8080";

pub async fn get_audio() -> Vec<types::AudioItem> {
    if WEB {
        if let Ok(data) = fetch::<Option<usize>, Vec<types::AudioItem>>(
            format!("{}{}", ROOT, "/get/get_audio"),
            None,
        )
        .await
        {
            data
        } else {
            Vec::new()
        }
    } else {
        types::load_audio()
    }
}
#[derive(Clone)]
pub enum Wrap {
    Db(Arc<Mutex<types::Db>>),
    Online(String),
}
impl Default for Wrap {
    fn default() -> Self {
        if WEB {
            Self::Online(ROOT.to_string())
        } else {
            Self::Db(Arc::new(Mutex::new(jagdlib::funcs::load(DATA))))
        }
    }
}
impl Wrap {
    pub fn get_arc(&self) -> Self {
        match self {
            Wrap::Db(db) => Wrap::Db(Arc::clone(db)),
            Wrap::Online(htt) => Wrap::Online(htt.to_string()),
        }
    }
}
pub async fn get_metadb(wrap: Wrap) -> Result<types::MetaDb, String> {
    match wrap {
        Wrap::Db(mutex) => {
            if let Ok(db) = mutex.lock() {
                db.get_metadb()
            } else {
                Err("mutex didn't open".to_string())
            }
        }
        Wrap::Online(htt) => {
            if let Ok(ok) = fetch::<Option<usize>, Result<types::MetaDb, String>>(
                format!("{}{}", htt, "/get/get_metadb"),
                None,
            )
            .await
            {
                ok
            } else {
                Err("Fetch failed".to_string())
            }
        }
    }
}
pub async fn get_metaquests(wrap: Wrap, mode: types::Mode) -> Result<types::MetaQuests, String> {
    match wrap {
        Wrap::Db(mutex) => {
            if let Ok(db) = mutex.lock() {
                db.get_metaquests(mode)
            } else {
                Err("mutex didn't open".to_string())
            }
        }
        Wrap::Online(htt) => {
            if let Ok(ok) = fetch::<types::Mode, Result<types::MetaQuests, String>>(
                format!("{}{}", htt, "/get/get_metaquests"),
                Some(mode),
            )
            .await
            {
                ok
            } else {
                Err("no response".to_string())
            }
        }
    }
}

pub async fn save(wrap: Wrap) -> Result<(), String> {
    match wrap {
        Wrap::Db(mutex) => {
            if let Ok(mut db) = mutex.lock() {
                db.save();
                db.save_to_disk(DATA.to_string()).unwrap();
            }
            Ok(())
        }
        Wrap::Online(htt) => {
            post::<Option<String>>(format!("{}{}", htt, "/mod/save"), Some(DATA.to_string()))
                .await
                .unwrap();
            Ok(())
        }
    }
}
pub async fn queue(wrap: Wrap, quest: jagdlib::types::Question) {
    match wrap {
        Wrap::Db(mutex) => {
            if let Ok(mut db) = mutex.lock() {
                db.queue(quest);
            }
        }
        Wrap::Online(htt) => {
            post(format!("{}{}", htt, "/mod/queue"), quest)
                .await
                .unwrap();
        }
    }
}
pub async fn next(wrap: Wrap) -> Result<jagdlib::types::Question, String> {
    match wrap {
        Wrap::Db(mutex) => {
            if let Ok(mut db) = mutex.lock() {
                db.next_one()
            } else {
                Err("Mutex didn't open".to_string())
            }
        }
        Wrap::Online(htt) => {
            if let Ok(ok) = fetch::<Option<usize>, Result<types::Question, String>>(
                format!("{}{}", htt, "/get/next"),
                None,
            )
            .await
            {
                ok
            } else {
                Err("Response failed".to_string())
            }
        }
    }
}
pub async fn set_mode(wrap: Wrap, mode: types::Mode) {
    match wrap {
        Wrap::Db(mutex) => {
            if let Ok(mut db) = mutex.lock() {
                db.set_mode(mode);
            }
        }
        Wrap::Online(htt) => {
            post(format!("{}{}", htt, "/mod/set_mode"), mode)
                .await
                .unwrap();
        }
    }
}
pub async fn _learn_history(wrap: Wrap) -> Result<types::LearnHistory, String> {
    match wrap {
        Wrap::Db(mutex) => {
            if let Ok(db) = mutex.lock() {
                db.learn_history()
            } else {
                Err("Mutex failed".to_string())
            }
        }
        Wrap::Online(htt) => {
            if let Ok(ok) = fetch::<Option<usize>, Result<types::LearnHistory, String>>(
                format!("{}{}", htt, "/get/learn_history"),
                None,
            )
            .await
            {
                ok
            } else {
                Err("Response failed".to_string())
            }
        }
    }
}
pub async fn _edit_history(wrap: Wrap) -> Result<types::EditHistory, String> {
    match wrap {
        Wrap::Db(mutex) => {
            if let Ok(db) = mutex.lock() {
                db.edit_history()
            } else {
                Err("Mutex failed".to_string())
            }
        }
        Wrap::Online(htt) => {
            if let Ok(ok) = fetch::<Option<usize>, Result<types::EditHistory, String>>(
                format!("{}{}", htt, "/get/edit_history"),
                None,
            )
            .await
            {
                ok
            } else {
                Err("Response failed".to_string())
            }
        }
    }
}
