//#[cfg(target_family = "unix")]
//use jagdlib::funcs::*;

use crate::{
    comps::{Quest, TopBar},
    wrapper::*,
    Common, Message,
};
use iced::{
    widget::{button, checkbox, column, pick_list, text, Column},
    Alignment, Command, Length,
};
use jagdlib::types::*;

#[derive(Debug, Clone)]
pub enum Mtrain {
    Checkbox(bool),
    Start,
    LoadedNext(Result<Question, String>),
    Setup(Result<MetaDb, String>),
    SetupQuests(Result<MetaQuests, String>),
    Qeditor,
    PickLection(String),
    PickTopic(String),
    Next,
    Home,
}

#[derive(Default)]
pub struct Tmenu {
    topic: Option<String>,
    lection: Option<String>,
}
#[derive(Default)]
pub struct Train {
    pub state: TrainSt,
    pub topbar: TopBar,
    pub substate: TrainCa,
}
impl Train {
    pub fn new(wrap: Wrap) -> Self {
        Train {
            state: TrainSt {
                wrap,
                ..Default::default()
            },
            ..Default::default()
        }
    }
}
pub enum TrainCa {
    Card(Tcard),
    Menu(Tmenu),
}
impl Default for TrainCa {
    fn default() -> Self {
        TrainCa::Menu(Tmenu::default())
    }
}
#[derive(Default)]
pub struct Tcard {
    pub card: Quest,
}
#[derive(Default)]
pub struct TrainSt {
    //themas:Vec<String>,
    check: bool,
    meta: Option<MetaDb>,
    quests: Option<MetaQuests>,
    wrap: Wrap,
    temp_q: Question,
    //value: f32,
    thema: usize,
    lection: usize,
}
impl Train {
    pub fn view(&self) -> Column<Message> {
        match self {
            Train {
                state: trainst,
                topbar,
                substate: TrainCa::Card(card),
            } => column![
                topbar.view(Message::Train(Mtrain::Home)),
                card.card.view(
                    trainst.temp_q.clone(),
                    (trainst.thema + 1).to_string(),
                    Message::Train(Mtrain::Next)
                ),
            ]
            .align_items(Alignment::Center)
            .width(Length::Units(600)),
            Train {
                state: trainst,
                topbar,
                substate: TrainCa::Menu(editor),
            } => {
                let mut lection_usize: Vec<usize> = Vec::new();
                let mut lection: Vec<String> = Vec::new();
                if let Some(ok) = &trainst.quests {
                    lection_usize.extend(ok.quests.clone());
                }
                for i in lection_usize {
                    lection.push((i).to_string());
                }

                let edi = column![
                    text("Topics:"),
                    pick_list(
                        if let Some(ok) = &trainst.meta {
                            ok.thema_names.clone()
                        } else {
                            vec![]
                        },
                        editor.topic.clone(),
                        |t| { Message::Train(Mtrain::PickTopic(t)) }
                    ),
                    text("Lection:"),
                    pick_list(lection, editor.lection.clone(), |t| {
                        Message::Train(Mtrain::PickLection(t))
                    }),
                    checkbox("Show all Quests", trainst.check, |b| {
                        Message::Train(Mtrain::Checkbox(b))
                    }),
                    button("Edit").on_press(Message::Train(Mtrain::Qeditor))
                ]
                .align_items(Alignment::Center)
                .spacing(20);
                column![topbar.view(Message::Common(Common::Home)), edi,]
                    .align_items(Alignment::Center)
                    .width(Length::Units(600))
            }
        }
    }
    pub fn update(&mut self, message: Mtrain) -> iced::Command<Message> {
        match message {
            Mtrain::Checkbox(b) => {
                self.state.check = b;
                if b {
                    Command::perform(
                        get_metaquests(self.state.wrap.get_arc(), Mode::Edit((0, 1))),
                        |guest| Message::Train(Mtrain::SetupQuests(guest)),
                    )
                } else {
                    Command::perform(
                        get_metaquests(self.state.wrap.get_arc(), Mode::Edit((0, 0))),
                        |guest| Message::Train(Mtrain::SetupQuests(guest)),
                    )
                }
            }
            Mtrain::Setup(var) => {
                if let Ok(s) = var {
                    self.state.meta = Some(s);
                    if let Some(ok) = &self.state.meta {
                        if !ok.thema_names.is_empty() {
                            if let TrainCa::Menu(edi) = &mut self.substate {
                                edi.topic = Some(ok.thema_names[0].clone());
                                //if self.state.meta.unwrap().thema_sizes[0] != 0 {
                                //    edi.lection = Some("1".to_string())
                                //}
                            }
                        }
                    }
                    if !self.state.meta.as_ref().unwrap().thema_names.is_empty() {
                        Command::perform(
                            get_metaquests(self.state.wrap.get_arc(), Mode::Edit((0, 0))),
                            |guest| Message::Train(Mtrain::SetupQuests(guest)),
                        )
                    } else {
                        Command::none()
                    }
                } else {
                    println!("setup failed{:?}", var);
                    Command::none()
                }
            }

            Mtrain::SetupQuests(var) => {
                if let Ok(s) = var {
                    self.state.quests = Some(s.clone());
                    if !&s.quests.is_empty() {
                        if let TrainCa::Menu(var) = &mut self.substate {
                            var.lection = Some(s.quests[0].to_string());
                        }
                        self.state.lection = s.quests[0] - 1;
                    }
                } else {
                    println!("eroor in Quest setup{:?}", var);
                }
                Command::none()
            }
            Mtrain::PickLection(t) => {
                if let TrainCa::Menu(edi) = &mut self.substate {
                    edi.lection = Some(t.clone())
                }
                self.state.lection = t.parse::<usize>().unwrap() - 1;
                Command::none()
            }
            Mtrain::PickTopic(t) => {
                if let TrainCa::Menu(edi) = &mut self.substate {
                    edi.topic = Some(t.clone())
                }
                if let Some(ok) = &self.state.meta {
                    self.state.thema = ok.thema_names.iter().position(|r| r == &t).unwrap();

                    Command::perform(
                        get_metaquests(
                            self.state.wrap.get_arc(),
                            Mode::Edit((self.state.thema, 0)),
                        ),
                        |guest| Message::Train(Mtrain::SetupQuests(guest)),
                    )
                } else {
                    Command::none()
                }
            }
            Mtrain::Qeditor => {
                if let TrainCa::Menu(_edi) = &self.substate {
                    let mut checked = [false; 6];
                    for (i, s) in self.state.temp_q.awnsers.iter().enumerate() {
                        checked[i] = s.2;
                    }
                    self.substate = TrainCa::Card(Tcard {
                        card: Quest {
                            is_checked: checked,
                        },
                    });
                }
                Command::perform(
                    set_mode(
                        self.state.wrap.get_arc(),
                        Mode::Edit((self.state.thema, self.state.lection)),
                    ),
                    |_quest| Message::Train(Mtrain::Start),
                )
            }
            Mtrain::Start => Command::perform(next(self.state.wrap.get_arc()), |quest| {
                Message::Train(Mtrain::LoadedNext(quest))
            }),
            Mtrain::Next => {
                let mut commands = vec![];
                if let TrainCa::Card(card) = &mut self.substate {
                    if card.card.is_checked == [false; 6] {
                    } else {
                        for (i, (_, _, _)) in self.state.temp_q.awnsers.clone().iter().enumerate() {
                            self.state.temp_q.awnsers[i].2 = card.card.is_checked[i];
                        }
                        let test: u64 = std::time::SystemTime::now()
                            .duration_since(std::time::SystemTime::UNIX_EPOCH)
                            .unwrap()
                            .as_secs();

                        self.state.temp_q.edit = Some(test);
                        commands.push(Command::perform(
                            queue(self.state.wrap.get_arc(), self.state.temp_q.clone()),
                            |_quest| Message::Loaded,
                        ));
                    }
                    card.card.is_checked = [false; 6];
                }
                commands.push(Command::perform(
                    save(self.state.wrap.get_arc()),
                    |_quest| Message::Loaded,
                ));
                commands.push(Command::perform(next(self.state.wrap.get_arc()), |quest| {
                    Message::Train(Mtrain::LoadedNext(quest))
                }));
                Command::batch(commands)
            }
            Mtrain::LoadedNext(s) => {
                if let Ok(s) = s {
                    self.state.temp_q = s;
                    if let TrainCa::Card(card) = &mut self.substate {
                        for (i, s) in self.state.temp_q.awnsers.iter().enumerate() {
                            card.card.is_checked[i] = s.2;
                        }
                    }
                } else {
                    println!("Loading next one failed{:?}", &s);
                }
                Command::none()
            }
            Mtrain::Home => {
                self.substate = TrainCa::default();
                if let Some(ok) = &self.state.quests {
                    if !ok.quests.is_empty() {
                        if let TrainCa::Menu(var) = &mut self.substate {
                            var.lection = Some(ok.quests[0].to_string());
                        }
                    }
                }
                if let Some(ok) = &self.state.meta {
                    if !ok.thema_names.is_empty() {
                        if let TrainCa::Menu(edi) = &mut self.substate {
                            edi.topic = Some(ok.thema_names[0].clone());
                        }
                    }
                }
                self.state.check = false;
                Command::perform(
                    get_metaquests(self.state.wrap.get_arc(), Mode::Edit((0, 0))),
                    |guest| Message::Train(Mtrain::SetupQuests(guest)),
                )
            }
        }
    }
}
