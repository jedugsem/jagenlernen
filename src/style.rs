use iced::button;


const STYLE : button::Style = button::Style{
     //shadow_offset: Vector,
     //background: Option<Background>,
     border_radius: 10.,
     border_width: 4.0,
     //border_color: Color,
     //text_color: Color,
};
pub struct Butt;

impl button::StyleSheet for Butt {
    fn active(&self) -> button::Style{
        STYLE
    }

    fn hovered(&self) -> button::Style { STYLE  }
    fn pressed(&self) -> button::Style { STYLE }
    fn disabled(&self) -> button::Style { STYLE }
}



mod style {
    use iced::{button, Background, Color, Vector};

    pub enum Button {
        Safe,
        Kat,
        Next,
        Back,
    }

    impl button::StyleSheet for Button {
        fn active(&self) -> button::Style {
            match self {
                Button::Safe => button::Style::default(),
                Button::Kat => button::Style {
                    background: Some(Background::Color(Color::from_rgb(
                        0.2, 0.2, 0.7,
                    ))),
                    border_radius: 10.0,
                    text_color: Color::WHITE,
                    ..button::Style::default()
                },
                Button::Next => button::Style {
                    text_color: Color::from_rgb(0.5, 0.5, 0.5),
                    ..button::Style::default()
                },
                Button::Back => button::Style {
                    background: Some(Background::Color(Color::from_rgb(
                        0.8, 0.2, 0.2,
                    ))),
                    border_radius: 5.0,
                    text_color: Color::WHITE,
                    shadow_offset: Vector::new(1.0, 1.0),
                    ..button::Style::default()
                },
            }
        }

        fn hovered(&self) -> button::Style {
            let active = self.active();

            button::Style {
                text_color: match self {
                    Button::Icon => Color::from_rgb(0.2, 0.2, 0.7),
                    Button::FilterActive => Color::from_rgb(0.2, 0.2, 0.7),
                    _ => active.text_color,
                },
                shadow_offset: active.shadow_offset + Vector::new(0.0, 1.0),
                ..active
            }
        }
    }
}
