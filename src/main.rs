mod comps;
mod funcs;
mod learn;
mod settings;
mod signs;
mod train;
mod wrapper;
use crate::{
    learn::*,
    settings::Settingst,
    signs::{Msigns, Signs},
    train::*,
};
use iced::{
    executor,
    theme::Theme,
    time,
    widget::{button, column, container, text},
    window, Alignment, Application, Command, Element, Length, Settings, Subscription,
};
use wrapper::*;

pub fn main() -> iced::Result {
    let win = window::Settings {
        size: (450, 700),
        resizable: true,
        ..Default::default()
    };
    let settings = Settings {
        id: Some("JagenLernen".to_string()),
        window: win,
        ..Default::default()
    };
    //App::run(Settings::default())
    Master::run(settings)
}

struct Master {
    app: App,
    _theme: Theme,
}
impl Default for Master {
    fn default() -> Self {
        Master {
            app: App::default(),
            _theme: Theme::Dark,
        }
    }
}
#[derive(Default)]
struct Home {}
enum App {
    Home(Home),
    Learn(Learn),
    Train(Train),
    Signs(Signs),
    Settings(Settingst),
}
impl Default for App {
    fn default() -> Self {
        App::Home(Home::default())
    }
}

#[derive(Debug, Clone)]
pub enum Message {
    Home(Mhome),
    Signs(Msigns),
    Common(Common),
    Train(Mtrain),
    Learn(Mlearn),
    Toggle(usize, bool),
    Loaded,
    LearnWarning,
}

#[derive(Debug, Clone, Copy)]
pub enum Mhome {
    Learn,
    Quit,
    Settings,
    Train,
    Signs,
}

#[derive(Debug, Clone, Copy)]
pub enum Common {
    Home,
    Back,
    Next,
}
impl Application for Master {
    type Message = Message;
    type Theme = Theme;
    type Executor = executor::Default;
    type Flags = ();
    fn subscription(&self) -> Subscription<Message> {
        match self.app {
            App::Learn(Learn {
                state: _,
                topbar: _,
                substate: LearnCa::Test(_),
            }) => time::every(std::time::Duration::from_millis(1000))
                .map(|_| Message::Learn(Mlearn::Tick)),
            _ => Subscription::none(),
        }
    }
    fn view(&self) -> Element<Message, iced::Renderer<Self::Theme>> {
        let content = match &self.app {
            App::Home(_home) => column![
                text("JagenLernen").size(50),
                button("Learn")
                    .width(Length::Units(150))
                    .on_press(Message::Home(Mhome::Learn)),
                button("Train")
                    .width(Length::Units(150))
                    .on_press(Message::Home(Mhome::Train)),
                button("Signs")
                    .width(Length::Units(150))
                    .on_press(Message::Home(Mhome::Signs)),
                button("Settings")
                    .width(Length::Units(150))
                    .on_press(Message::Home(Mhome::Settings)),
                button("Quit")
                    .width(Length::Units(150))
                    .on_press(Message::Home(Mhome::Quit)),
            ]
            .align_items(Alignment::Center)
            .spacing(50)
            .padding(20),
            App::Signs(signs) => signs.view(),
            App::Learn(learn) => learn.view(),
            App::Train(train) => train.view(),
            App::Settings(settings) => settings.view(),
        };
        container(content).width(Length::Fill).center_x().into()
    }
    fn update(&mut self, message: Message) -> Command<Message> {
        match message {
            Message::Home(home) => {
                match home {
                    Mhome::Signs => {
                        *self = Master {
                            app: App::Signs(Signs::default()),
                            ..Default::default()
                        };
                        Command::perform(get_audio(), |au| Message::Signs(Msigns::Audio(au)))
                    }
                    Mhome::Learn => {
                        let wrap = Wrap::default();
                        *self = Master {
                            app: App::Learn(Learn::new(wrap.get_arc())),
                            ..Default::default()
                        };
                        Command::perform(get_metadb(wrap.get_arc()), |kats| {
                            Message::Learn(Mlearn::Setup(kats))
                        })
                    }
                    Mhome::Train => {
                        let wrap = Wrap::default();
                        *self = Master {
                            app: App::Train(Train::new(wrap.get_arc())),
                            ..Default::default()
                        };
                        Command::batch([Command::perform(get_metadb(wrap.get_arc()), |kats| {
                            Message::Train(Mtrain::Setup(kats))
                        })])
                    }
                    Mhome::Settings => {
                        *self = Master {
                            app: App::Settings(Settingst::default()),
                            ..Default::default()
                        };
                        Command::none()
                    }
                    Mhome::Quit => {
                        std::process::exit(15);
                        //Command::none()
                    }
                }
            }
            Message::Signs(message) => {
                if let App::Signs(signs) = &mut self.app {
                    signs.update(message)
                } else {
                    Command::none()
                }
            }
            Message::Common(Common::Home) => {
                *self = Master::default();
                Command::none()
            }
            Message::Common(Common::Back) => {
                *self = Master::default();
                Command::none()
            }
            Message::Train(train) => {
                if let App::Train(state) = &mut self.app {
                    state.update(train)
                } else {
                    Command::none()
                }
            }
            Message::Learn(learn) => {
                if let App::Learn(state) = &mut self.app {
                    state.update(learn)
                } else {
                    Command::none()
                }
            }

            Message::Toggle(i, is) => {
                if let App::Train(Train {
                    state: _,
                    topbar: _,
                    substate: TrainCa::Card(card),
                }) = &mut self.app
                {
                    card.card.is_checked[i] = is;
                } else if let App::Learn(Learn {
                    state: _,
                    topbar: _,
                    substate: LearnCa::Card(card),
                }) = &mut self.app
                {
                    card.card.is_checked[i] = is;
                }

                Command::none()
            }
            _ => Command::none(),
        }
    }
    fn new(_flags: ()) -> (Self, Command<Message>) {
        (
            Master {
                ..Default::default()
            },
            Command::none(),
        )
    }
    fn title(&self) -> String {
        String::from("JagenLernen")
    }
    fn theme(&self) -> Self::Theme {
        //Self::Theme::default()
        self._theme
    }
}
